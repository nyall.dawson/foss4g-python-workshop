README

Before the workshop if you are running 3.4 you will need to 
patch a file using the PatchQGIS.bat file.

This batch file will replace the processing script editor to fix a font 
issue that was found in 3.4.

If you are running 3.2 this will not need to be patched.

<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" simplifyMaxScale="1" labelsEnabled="0" readOnly="0" simplifyDrawingTol="1" maxScale="0" version="3.4.0-Madeira" simplifyLocal="1" simplifyDrawingHints="1" minScale="1e+8" simplifyAlgorithm="0" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" enableorderby="0" symbollevels="0" type="singleSymbol">
    <symbols>
      <symbol clip_to_extent="1" alpha="1" type="line" name="0">
        <layer class="SimpleLine" locked="0" enabled="1" pass="0">
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="35,35,35,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.86"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer class="MarkerLine" locked="0" enabled="1" pass="0">
          <prop k="interval" v="3"/>
          <prop k="interval_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="interval_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_along_line" v="0"/>
          <prop k="offset_along_line_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_along_line_unit" v="MM"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="placement" v="lastvertex"/>
          <prop k="rotate" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" alpha="1" type="marker" name="@0@1">
            <layer class="SimpleMarker" locked="0" enabled="1" pass="0">
              <prop k="angle" v="0"/>
              <prop k="color" v="255,0,0,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2.6"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" name="name" value=""/>
                  <Option name="properties"/>
                  <Option type="QString" name="type" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory minScaleDenominator="0" sizeScale="3x:0,0,0,0,0,0" width="15" rotationOffset="270" sizeType="MM" penWidth="0" scaleBasedVisibility="0" backgroundColor="#ffffff" penAlpha="255" height="15" backgroundAlpha="255" diagramOrientation="Up" barWidth="5" enabled="0" maxScaleDenominator="1e+8" lineSizeType="MM" penColor="#000000" scaleDependency="Area" minimumSize="0" labelPlacementMethod="XHeight" lineSizeScale="3x:0,0,0,0,0,0" opacity="1">
      <fontProperties style="" description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" dist="0" priority="0" obstacle="0" placement="2" showAll="1" zIndex="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="AssetID">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Road_Name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Road_Type">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Suffix">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Locality">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="From_Road">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="To_Road">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Length_m">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Speed">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Class">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Hierarchy">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Authority">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Road_Label">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="fid" index="0" name=""/>
    <alias field="AssetID" index="1" name=""/>
    <alias field="Road_Name" index="2" name=""/>
    <alias field="Road_Type" index="3" name=""/>
    <alias field="Suffix" index="4" name=""/>
    <alias field="Locality" index="5" name=""/>
    <alias field="From_Road" index="6" name=""/>
    <alias field="To_Road" index="7" name=""/>
    <alias field="Length_m" index="8" name=""/>
    <alias field="Speed" index="9" name=""/>
    <alias field="Class" index="10" name=""/>
    <alias field="Hierarchy" index="11" name=""/>
    <alias field="Authority" index="12" name=""/>
    <alias field="Road_Label" index="13" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" field="fid" expression=""/>
    <default applyOnUpdate="0" field="AssetID" expression=""/>
    <default applyOnUpdate="0" field="Road_Name" expression=""/>
    <default applyOnUpdate="0" field="Road_Type" expression=""/>
    <default applyOnUpdate="0" field="Suffix" expression=""/>
    <default applyOnUpdate="0" field="Locality" expression=""/>
    <default applyOnUpdate="0" field="From_Road" expression=""/>
    <default applyOnUpdate="0" field="To_Road" expression=""/>
    <default applyOnUpdate="0" field="Length_m" expression=""/>
    <default applyOnUpdate="0" field="Speed" expression=""/>
    <default applyOnUpdate="0" field="Class" expression=""/>
    <default applyOnUpdate="0" field="Hierarchy" expression=""/>
    <default applyOnUpdate="0" field="Authority" expression=""/>
    <default applyOnUpdate="0" field="Road_Label" expression=""/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" notnull_strength="0" field="fid" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="AssetID" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="Road_Name" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="Road_Type" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="Suffix" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="Locality" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="From_Road" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="To_Road" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="Length_m" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="Speed" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="Class" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="Hierarchy" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="Authority" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" notnull_strength="0" field="Road_Label" constraints="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" field="fid" exp=""/>
    <constraint desc="" field="AssetID" exp=""/>
    <constraint desc="" field="Road_Name" exp=""/>
    <constraint desc="" field="Road_Type" exp=""/>
    <constraint desc="" field="Suffix" exp=""/>
    <constraint desc="" field="Locality" exp=""/>
    <constraint desc="" field="From_Road" exp=""/>
    <constraint desc="" field="To_Road" exp=""/>
    <constraint desc="" field="Length_m" exp=""/>
    <constraint desc="" field="Speed" exp=""/>
    <constraint desc="" field="Class" exp=""/>
    <constraint desc="" field="Hierarchy" exp=""/>
    <constraint desc="" field="Authority" exp=""/>
    <constraint desc="" field="Road_Label" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column hidden="0" width="-1" type="field" name="fid"/>
      <column hidden="0" width="-1" type="field" name="AssetID"/>
      <column hidden="0" width="-1" type="field" name="Road_Name"/>
      <column hidden="0" width="-1" type="field" name="Road_Type"/>
      <column hidden="0" width="-1" type="field" name="Suffix"/>
      <column hidden="0" width="-1" type="field" name="Locality"/>
      <column hidden="0" width="-1" type="field" name="From_Road"/>
      <column hidden="0" width="-1" type="field" name="To_Road"/>
      <column hidden="0" width="-1" type="field" name="Length_m"/>
      <column hidden="0" width="-1" type="field" name="Speed"/>
      <column hidden="0" width="-1" type="field" name="Class"/>
      <column hidden="0" width="-1" type="field" name="Hierarchy"/>
      <column hidden="0" width="-1" type="field" name="Authority"/>
      <column hidden="0" width="-1" type="field" name="Road_Label"/>
      <column hidden="1" width="-1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="AssetID"/>
    <field editable="1" name="Authority"/>
    <field editable="1" name="Class"/>
    <field editable="1" name="From_Road"/>
    <field editable="1" name="Hierarchy"/>
    <field editable="1" name="Length_m"/>
    <field editable="1" name="Locality"/>
    <field editable="1" name="Road_Label"/>
    <field editable="1" name="Road_Name"/>
    <field editable="1" name="Road_Type"/>
    <field editable="1" name="Speed"/>
    <field editable="1" name="Suffix"/>
    <field editable="1" name="To_Road"/>
    <field editable="1" name="fid"/>
  </editable>
  <labelOnTop>
    <field name="AssetID" labelOnTop="0"/>
    <field name="Authority" labelOnTop="0"/>
    <field name="Class" labelOnTop="0"/>
    <field name="From_Road" labelOnTop="0"/>
    <field name="Hierarchy" labelOnTop="0"/>
    <field name="Length_m" labelOnTop="0"/>
    <field name="Locality" labelOnTop="0"/>
    <field name="Road_Label" labelOnTop="0"/>
    <field name="Road_Name" labelOnTop="0"/>
    <field name="Road_Type" labelOnTop="0"/>
    <field name="Speed" labelOnTop="0"/>
    <field name="Suffix" labelOnTop="0"/>
    <field name="To_Road" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>fid</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>

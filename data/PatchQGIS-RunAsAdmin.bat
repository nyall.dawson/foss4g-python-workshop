@ECHO OFF
SET ROOT=C:\Program Files (x86)\QGIS 3.4
IF EXIST "%ROOT%\apps\qgis\python\plugins\processing\script" (
   copy %~dp0ScriptEdit.py "%ROOT%\apps\qgis\python\plugins\processing\script"
   echo "Patched"
   GOTO end
)

SET ROOT=C:\Program Files\QGIS 3.4
IF EXIST "%ROOT%\apps\qgis\python\plugins\processing\script" (
   copy %~dp0ScriptEdit.py "%ROOT%\apps\qgis\python\plugins\processing\script"
   echo "Patched"
   GOTO end
)

ECHO Can't find QGIS install. Update %%ROOT%% in this file to change install location

:end
pause
exit
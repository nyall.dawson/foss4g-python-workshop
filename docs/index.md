# Workshop Overview

All the OTHER places you will love using Python in QGIS FOSS4G workshop

Online version of the guide can be found [here]()

## What are we going to cover in this workshop

- The Python Console and editor
- Startup Scripts
- Custom Expression Functions
    - Style function
    - Layout function
- Processing Scripts
- Project Macros

## What stuff do we need?

QGIS 3.4 is the best because it's a bigger number (and the examples will work better)

The [dataset and project can be found here](https://github.com/NathanW2/foss4g-python-workshop/blob/master/Python-Workshop-NathanW.zip)

Data set includes some layers and project for the workshop.  A layout is also included to use a template for a later example.

We will only be using this project though the workshop.

